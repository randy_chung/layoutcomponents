import { StatusBar } from 'expo-status-bar';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import SkgHeader from './components/SkgAppbarHeader';
import SkgBottomNav from './components/SkgBottomNav';

export default function App() {
  return (
    <>
      <SkgHeader />

      <SkgBottomNav />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
