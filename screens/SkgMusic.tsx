import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import { Button, Card, Paragraph, Title } from 'react-native-paper';

import { SkgContainer } from '../components/SkgContainer';
import { SkgColumn } from '../components/SkgColumn';
import { SkgRow } from '../components/SkgRow';

export default function SkgMusic() {
  return (
    <SkgContainer>
      <SkgColumn gap={10}>
        <SkgRow className={'mainContent'} showBorders hRight vCenter>
          <Text style={{ backgroundColor: 'grey' }}>Image</Text>
          <SkgColumn className={'titleAndDescription'} gap={15}>
            <Text>Top Title</Text>
            <Text>Bottom Description</Text>
          </SkgColumn>
        </SkgRow>
        <SkgRow className={'dateAndChevron'} hSpaceBetween>
          <Text style={{ color: 'red' }}>Date</Text>
          <Text>&gt;</Text>
        </SkgRow>

        <SkgColumn gap={30}>
          <Text style={{ flexGrow: 1 }}>
            Bottom Description Bottom Description Bottom Description Bottom Description Bottom
            Description Bottom Description Bottom Description Bottom Description Bottom Description
            Bottom Description Bottom Description Bottom Description Bottom Description Bottom
            Description Bottom Description Bottom Description{' '}
          </Text>
        </SkgColumn>

        <SkgRow gap={30}>
          <Text style={{ flexGrow: 1 }}>
            Bottom Description Bottom Description Bottom Description Bottom Description Bottom
            Description Bottom Description Bottom Description Bottom Description Bottom Description
            Bottom Description Bottom Description Bottom Description Bottom Description Bottom
            Description Bottom Description Bottom Description{' '}
          </Text>
        </SkgRow>
      </SkgColumn>
    </SkgContainer>
  );
}
